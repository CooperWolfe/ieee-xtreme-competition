# Infinite Snakes and Ladders

## Problem
The problem was to create a snakes and ladders simulator for arbitrarily sized boards. When a player wins, a message should be output that a player won. If there are still players, the remaining rolls should be played for them, excluding all previous winners. If the game concludes before a player reaches the finish, their position should be output

## Note about solution
For the sake of time, I programmed an overly complex solution that uses two dimensions. A better solution would have been to create a one-dimensional board, map the shoots and ladders to positions in that one dimension, and simulate a one-dimensional shoots and ladders game.

## Solution
Plain and simple, I simulate the game with logic for turning at the ends of the board and travelling on snakes and ladders.
