# Binary Tree Generation

## Problem
Given the infix (left-to-right) and prefix (depth-first) iterations of a binary tree, output the tree in a readable format as quickly as possible

## Approach
This algorithm generates all possible representations of the tree's prefix then compares each generation to the infix and outputs the solution

I will use a linked list of binary trees, iterate over them, depth-first searching for each successive node in the prefix. Once it is found, the depth-first iteration continues, but now each visit to a null node will append another tree to the linked list with the next node in the prefix. Continuing this process until all nodes have been generated will result in a linked list of all representations of the prefix as well as some extras in the beginning with a shorter size than expected. These will from now on be ignored.
The generation will take about O(nlogn) time.

Iterating again over the list of binary trees, I compare each correct-size solution to the infix representation. If the tree matches, I will output it.
Iterating over O(nlogn) solutions and in each one visiting worst-case n nodes, this algorithm takes best-case exponential time and worst case O(n^2logn) time.

Outputting the correct answer is done by knowing only the depth of each node and size of the whole tree. The size inferred from the input (constant time) and the depth is calculated via depth-first iteration (linear time) and stored in a dictionary. Then, a 2d array is constructed, depth X size in dimensions, and the infix representation is iterated. For each letter in the infix notation, we get the depth of the node from the dictionary (constant time) and put it in position (iteration, depth-1) of the 2d Array. For each undefined node in the 2d array, two spaces are printed and for each non-null element, that element and an additional space is printed.
This printing algorithm will take linear time.

Thus, the overall performance of this algorithm is at best exponential and likely O(n^2logn).

## Organization
Because the competition only allowed for one file submissions, organization was not a priority.
